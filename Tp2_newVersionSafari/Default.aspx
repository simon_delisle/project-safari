﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Tp2_newVersionSafari.Default" %>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="strict-origin" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <title>Safari Adventure</title>
  </head>
  <body>
      
      
   <%-- <form id="form1" runat="server" class="container">--%>
        <div class="row">
            <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

            <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="<%=about %>">About</a>
                                    <a class="nav-item nav-link" href="<%=pages %>">Pages</a>
                                    <a class="nav-item nav-link " href="<%=gallery %>">Gallery</a>
                                    <a class="nav-item nav-link " href="<%=blog %>">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a>
                                <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                 <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete"></i><%= titre1 %></h1>
                        </div>
                        <div id="btn" class="col-2 titre">
                            <form>
                                <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>

                            </form>
                            
                        </div>

                            </div>

                        </div>
                        
                    </div>
                 <%--carrousel images principales avec titre centré --%>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                    <div class="carousel-item active ">
                            <img class="d-block w-100" src="./images/<%=image1 %>" alt="First slide"/>
                       
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="./images/<%=image5 %>" alt="Second slide"/>
                       
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="./images/<%=image6 %>" alt="Third slide"/>
                    
                    </div>
                    </div>
                    <div id="blockText1">
                             <h1 id="titrePrincipal"><%=soustitre1 %></h1>
                             <h3 id="soustitrePrincipal"><%=texte1 %></h3>
                    </div>
                      
                   
                </div>


                 
                 <%--les cards du bas --%>
                    <div class="row cards rowCartes"  >

                        
                        <div class="col-sm-4  card cartes" >
                            
                            <div class="card">
                                    <div class="card-header">
                                        <h3><%=titre2 %></h3>
                                    </div>
                                    <img class="card-img-top" src="./images/<%=image2 %>"/>
                                    <div class="card-body">
                                    <h5 class="card-title"><%=soustitre2 %></h5>
                                    <p class="card-text"><%=texte2 %></p>
                                    <a href="#" class="btn btn-warning">more</a>
                                    </div>
                            </div>
                        </div>
                        <div class="col-sm-4 card tsts cartes">
                            <div class="card" >
                                <div class="card-header">
                                        <h3><%=titre3 %></h3>
                                    </div>
                                    <img class="card-img-top" src="./images/<%=image3 %>"/>
                            <div class="card-body">
                                    <h5 class="card-title"><%=soustitre3 %></h5>
                                    <p class="card-text"><%=texte3 %></p>
                                    <a href="#" class="btn btn-warning">more</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-4 card  cartes" >
                            <div class="card" >
                                <div class="card-header">
                                        <h3><%=titre4 %></h3>
                                    </div>
                                    <img class="card-img-top" src="./images/<%=image4 %>"/>
                            <div class="card-body">
                                    <h5 class="card-title"><%=soustitre3 %></h5>
                                    <p class="card-text"><%=texte3 %></p>
                                    <a href="#" class="btn btn-warning">more</a>
                            </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="col-2 d-none d-lg-block tests"></div>
   
        </div>
<%--      </form>--%>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
