﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="email.aspx.cs" Inherits="Tp2_newVersionSafari.email" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="strict-origin" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style2.css" rel="stylesheet" />
 

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <title></title>
</head>
<body>
    <div class="row">
           <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

         <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="">About</a>
                                    <a class="nav-item nav-link" href="">Pages</a>
                                    <a class="nav-item nav-link " href="">Gallery</a>
                                    <a class="nav-item nav-link " href="">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a> 
                                    <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                  <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete"></i>Safari Adventure</h1>
                                </div>
                                <div id="btn" class="col-2 titre">      
                                     <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>                   
                                </div>
                            </div>
                        </div>            
                    </div>

                <%-- principal admin--%>

                  <div class ="row" id="containerContact">
                        <div class="col-12" id="infosContact">
                            <div class="row">
                                <div class="col-12">
                                    <h1 id="titreAdmin">Page Administrateur</h1>
                                    <h5>Configuration des éléments présents sur le site ce passe ici !!!</h5>
                                    <h5>Veuillez cliquez sur une des options suivantes : </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                     <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup2">
                                        <div class="navbar-nav">
                                                <a class="nav-item nav-link active" href="admin.aspx">Accueil <span class="sr-only">(current)</span></a>
                                                <a class="nav-item nav-link" href="email.aspx">Email</a>
                                                <a class="nav-item nav-link" href="">Voyages</a>

                                        </div>
                                        </div>
                                    </nav>

                                    <%-- email  --%>
                                    <div class="row">
                                        
                                      
                                        <div class="col-12">
                                            <form runat="server">
                                            <asp:GridView ID="GridViewEmails" runat="server" AutoGenerateColumns="false" Height="202px" Width="1142px">
                                                  <Columns>
                                                      <asp:BoundField DataField="nom" HeaderText ="nom" />
                                                      <asp:BoundField DataField="email" HeaderText ="email" />
                                                      <asp:BoundField DataField="tel" HeaderText ="tel" />
                                                      <asp:BoundField DataField="message" HeaderText ="message" />
                                                      <asp:TemplateField>
                                                          <ItemTemplate>
                                                              <asp:LinkButton ID="lnkSelect" Text="Repondre" runat="server" CommandArgument='<%# Eval("email") %>' OnClick="reply_click" />
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                  </Columns>
                                            </asp:GridView>
                                                <br />
                                            
                                                <br />
                                                <h6>Répondre au message client sur le courriel :</h6>
                                                <asp:Label ID="LabelEmail" runat="server" Text="" Visible="false"></asp:Label>
                                                <br />
                                            <textarea id="boitereponse" runat="server" style="margin-top: 5%; height: 50%; width: 50%" visible="False" ></textarea>
                                            <asp:Button ID="ButtonEnvoyer" runat="server" Text="Envoyer" Height="35px" Width="93px" OnClick="ButtonEnvoyer_Click" Visible="False"/>
                                            </form>
                                        </div>
                                        
                                    </div>
                              </div><%-- fin row --%>
                        </div> <%-- fin col --%>
                    </div><%-- fin row --%>
                </div>
            </div>
            <div class="col-2 d-none d-lg-block tests"></div>
        </div>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>

 