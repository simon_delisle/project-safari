﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Tp2_newVersionSafari
{
    public partial class email : System.Web.UI.Page
    {
        private string adresseCourriel;
        SafariDbDataContext db = new SafariDbDataContext();

        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Session3\AT1\Tp2_newVersionSafariii\Tp2_newVersionSafari\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var res = from u in db.user
                          select u;


                GridViewEmails.DataSource = res;
                GridViewEmails.DataBind();




            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }


        protected void reply_click(object sender, EventArgs e)
        {
            var closeLink = (Control)sender;
            GridViewRow row = (GridViewRow)closeLink.NamingContainer;
            adresseCourriel = row.Cells[1].Text;



            LabelEmail.Text = adresseCourriel;
            LabelEmail.Visible = true;
            boitereponse.Visible = true;
            ButtonEnvoyer.Visible = true;
            SetFocus(boitereponse);
        }

        protected void ButtonEnvoyer_Click(object sender, EventArgs e)
        {
            string adresseCourriel = LabelEmail.Text;
            string reponse = boitereponse.Value;


            if (sendMail(new Email { To = adresseCourriel, Sujet = "Réponse a votre demande d'information", Message = reponse }))
            {
                //Response.Write("Email Envoyé");
                boitereponse.Visible = true;
                ButtonEnvoyer.Visible = true;
                Console.WriteLine("Email Envoye");


            }

            else
            {
                Response.Write(adresseCourriel);
                boitereponse.Visible = true;
                ButtonEnvoyer.Visible = true;

            }
        }

        private bool sendMail(Email email)
        {
            bool status = false;

            try
            {
                //on met ici l'adresse de l'envoyeur 
                //idealement on devrait mettre ces infos dans la database et faire une requete pour les recuperer
                // pour eviter d'afficher ces infos dans le code (adresse et mot de passe)
                string MailSender = "simondelisle@teccart.online";
                string MailPw = "";

                SmtpClient smtpclient = new SmtpClient("smtp.office365.com", 587);
                smtpclient.Timeout = 100000;
                smtpclient.EnableSsl = true;
                smtpclient.UseDefaultCredentials = false;
                smtpclient.Credentials = new NetworkCredential(MailSender, MailPw);


                MailMessage mailMessage = new MailMessage(MailSender, email.To, email.Sujet, email.Message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpclient.Send(mailMessage);

                status = true;


                return status;


            }
            catch (Exception ex)
            {
                return status;

            }
        }








    }
}