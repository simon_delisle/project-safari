﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using System.Data;


namespace Tp2_newVersionSafari
{
    public partial class voyage1 : System.Web.UI.Page
    {
        SafariDbDataContext db = new SafariDbDataContext();

        public string image1, image2, image3, image4, image5, image6, background;
        public string home, about, pages, blog, gallery, contact, admin;
        public string titre1, titre2, titre3, titre4, titre5, titre6;
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Session3\AT1\Tp2_newVersionSafari\Tp2_newVersionSafari\App_Data\Database.mdf;Integrated Security=True");


            if (!IsPostBack)
            {
                //dropdown pour la selection d'images -- bind avec le dossier images----------------
                DirectoryInfo dir = new DirectoryInfo(MapPath("~/images/"));
                FileInfo[] imagenames = dir.GetFiles();
                ArrayList listeImg = new ArrayList();
                foreach (FileInfo info in imagenames)
                {
                    listeImg.Add(info);

                }

                DropDownListImages.DataSource = listeImg;
                DropDownListImages.DataBind();

            }
        }

        protected void BtnCreate_Click(object sender, EventArgs e)
        {
            voyages voyage = new voyages();

            voyage.titre = TextBoxTitre.Text;
            voyage.soustitre = TextBoxSousTitre.Text;
            voyage.description = TextBoxDescription.Text;
            voyage.imageNom = DropDownListImages.SelectedValue;

            db.voyages.InsertOnSubmit(voyage);
            db.SubmitChanges();

            ScriptManager.RegisterStartupScript(this.DropDownListImages, typeof(string), "Alert", "alert('Voyage bien créé! ');", true);


        }



        protected void DropDownListImages_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dossier = "./images/";
            ImageVoyage.ImageUrl = dossier + DropDownListImages.SelectedValue;

        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {

            TextBoxTitre.Text = "";
            TextBoxSousTitre.Text = "";
            TextBoxDescription.Text = "";

        }
        protected void ButtonUpdate_click(object sender, EventArgs e)
        {

          
            db.ExecuteCommand("TRUNCATE TABLE cards");

            int compteur = 0;

            if (compteur < 4)
            {
                foreach (GridViewRow row in GridViewChoix.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox checkImg = row.Cells[5].FindControl("CheckBoxAdd") as CheckBox;

                        if (checkImg.Checked)
                        {
                            string titre = row.Cells[1].Text;
                            string soustitre = row.Cells[2].Text;
                            string description = row.Cells[3].Text;
                            string nomImage = row.Cells[4].Text;
                            cards newcard = new cards { titre = titre, soustitre = soustitre, description = description, imageNom = nomImage };


                            db.cards.InsertOnSubmit(newcard);
                            db.SubmitChanges();
                            compteur += 1;

                        }

                    }

                }




                Response.Write("<script>alert('Les voyages ont bien été changés ! ');</script>");

            }
            else
            {
                Response.Write("<script>alert('Maximum 3 images svp');</script>");
            }


        }
    }
}