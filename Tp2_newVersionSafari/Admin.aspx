﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Tp2_newVersionSafari.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="strict-origin" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <title></title>
</head>
<body>
    <div class="row">
           <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

         <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="">About</a>
                                    <a class="nav-item nav-link" href="">Pages</a>
                                    <a class="nav-item nav-link " href="">Gallery</a>
                                    <a class="nav-item nav-link " href="">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a> 
                                    <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                  <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete"></i>Safari Adventure</h1>
                                </div>
                                <div id="btn" class="col-2 titre">      
                                     <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>                   
                                </div>
                            </div>
                        </div>            
                    </div>

                <%-- principal admin--%>

                  <div class ="row" id="containerContact">
                        <div class="col-12" id="infosContact">
                            <div class="row">
                                <div class="col-12">
                                    <h1 id="titreAdmin">Page Administrateur</h1>
                                    <h5>Configuration des éléments présents sur le site ce passe ici !!!</h5>
                                    <h5>Veuillez cliquez sur une des options suivantes : </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                     <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup2">
                                        <div class="navbar-nav">
                                                <a class="nav-item nav-link active" href="admin.aspx">Home <span class="sr-only">(current)</span></a>
                                                <a class="nav-item nav-link" href="email.aspx">Email</a>
                                                <a class="nav-item nav-link" href="voyage.aspx">Voyages</a>

                                        </div>
                                        </div>
                                    </nav>
                                     
                                    <%--section upload et images --%>
                                    <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col">
                                                <br />
                                                <br />
                                                <h6> Gestion des images du carouselle de la page Index(default.aspx)</h6>
                                                <hr />
                                                 <br />
                                            </div>
                                        </div>
                                       <form id="Form1" method="post" enctype="multipart/form-data" runat="server">
                                       <div class="row">
                                            <div class="col-6">
                                        <%-- Gridview contenant les images du dossier image --%>
                                         <%--//////////////////////gridview pour display les images et noms d'images + radiobutton pour actif/inactif////////////////////////////// --%>
                                                   <%--gridview avec images  --%>
                                                    <h3>Images disponibles :</h3>  
                                                        <table>  
                                                            <tr>  
                                                                <td>  
                                                                    Fichier:  
                                                                </td>  
                                                                <td>  
                                                                   <input type="file" id="File1" name="File1" runat="server" />
                                                                </td>  
                                                            </tr>  
                                                            <tr>  
                                                                <td>   
                                                                </td>  
                                                                <td>  
                                                                    <asp:Button ID="ButtonUpload" runat="server" Text="Upload" OnClick="ButtonUpload_Click" />
                                                                </td> 
                                                            </tr>  
                                                        </table>  
                                                        <br />
                                                         <asp:GridView ID="GridViewImages" CssClass="grid" runat="server" AutoGenerateColumns="false"  ShowHeader="false">  
                                                              <Columns>  
                                                                    <asp:BoundField DataField="Text" HeaderText="nom" />  
                                                                    <asp:ImageField DataImageUrlField="Value" ControlStyle-Height="230" ControlStyle-Width="400" HeaderText="Images" />  
                                                                    <asp:TemplateField HeaderText ="1er Item du carousel" >
                                                                       <ItemTemplate>
                                                                           <asp:CheckBox ID="CheckBoxAdd" runat="server" />
                                                                       </ItemTemplate>
                                                                       <ItemStyle Width="50px" />
                                                                   </asp:TemplateField>
                                                              </Columns>  
                                                         </asp:GridView> 
                                                           
                                                
                                                    </div>
                                                    <div class="col-6">
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <br />

                                                        <h6>Cliquez pour mettre a jour le carousel avec les nouveaux voyage voulu :</h6>
                                                        <asp:Button ID="ButtonUpdate" runat="server" Text="Update Carousel" OnClick="ButtonUpdate_click"/>
                                                   </div>
                                          
                                            </div>
                                      </form>
                                   </div>
                                     <%-- fin section images --%>
                                            
                               </div><%--fin col-12 --%>
                                                   
                             
                           </div><%-- fin row --%>
                        </div> <%-- fin col --%>
                    </div><%-- fin row --%>
                </div>
            </div>
            <div class="col-2 d-none d-lg-block tests"></div>
        </div>
    
   
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>        
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="./script/Javascript.js"></script>
</body>
</html>
